package xyz.znix.shadertool

import java.nio.ByteBuffer

typealias RefMap = HashMap<Int, PersistentObject>

abstract class PersistentObject(val hdr: ObjectHeader?) {
    abstract fun load(bb: ByteBuffer, refMap: RefMap)
    abstract fun save(bb: ByteBuffer)
}

data class ObjectHeader(val type: Int, val refId: Int, val len: Int, val pos: Int) {
    val end get() = pos + len

    override fun toString(): String {
        val typeStr = (type.toLong() and 0xffffffffL).toString(16)
        return "PersistentObject(type=$typeStr, refId=$refId, len=$len, pos=$pos)"
    }

    companion object {
        // See dsl::wd3d::D3DShaderManager::D3DShaderManager
        const val SHADER = 0x7F3552D1
        const val SHADER_PASS = 0x214b1aaf
        const val SHADER_LIBRARY = 0x12812C1A
    }

    fun build(): PersistentObject {
        return when (type) {
            SHADER -> ObjShader(this)
            SHADER_PASS -> ObjShaderPass(this)
            SHADER_LIBRARY -> ObjShaderLibrary(this)
            else -> error("Unknown tag ID $type")
        }
    }
}

class ObjShader(hdr: ObjectHeader?) : PersistentObject(hdr) {
    val shaderPacks = HashMap<Idstring, Entry>()

    override fun load(bb: ByteBuffer, refMap: RefMap) {
        val len = bb.int

        for (i in 1..len) {
            val idstring = Idstring(bb.long)

            val passesLen = bb.int
            val passes = (1..passesLen).map {
                val refId = bb.int
                return@map refMap[refId] as ObjShaderPass
            }

            shaderPacks[idstring] = Entry(passes, i)
        }
    }

    override fun save(bb: ByteBuffer) {
        bb.putInt(shaderPacks.size)

        // See the comment in ObjShaderLibrary.save for info about sorting
        // Oddly it seems they're sorted in reverse sometimes, so store the IDs they appear in
        for ((id, shaders) in shaderPacks.entries.sortedBy { it.value.sortIdx }) {
            bb.putLong(id.hash)

            bb.putInt(shaders.passes.size)
            for (shader in shaders.passes) {
                bb.putRefId(shader)
            }
        }
    }

    data class Entry(val passes: List<ObjShaderPass>, val sortIdx: Int = -1)
}

class ObjShaderPass(hdr: ObjectHeader?) : PersistentObject(hdr) {
    val stateVars = ArrayList<StateVar>()
    val textures = ArrayList<TextureBlock>()

    var vertexShader: ByteArray? = null
    var fragmentShader: ByteArray? = null

    override fun load(bb: ByteBuffer, refMap: RefMap) {
        val lenA = bb.int
        for (i in 1..lenA) {
            stateVars += StateVar.load(bb)
        }

        val lenB = bb.int
        for (i in 1..lenB) {
            val varA = bb.int
            val uniformName = bb.getCString()

            val innerLen = bb.int
            val innerVars = (1..innerLen).map { StateVar.load(bb) }

            textures += TextureBlock(varA, uniformName, innerVars)
        }

        // Shader programs, length-prefixed
        vertexShader = bb.getLenArray()
        fragmentShader = bb.getLenArray()
    }

    override fun save(bb: ByteBuffer) {
        bb.putInt(stateVars.size)
        for (sv in stateVars) {
            sv.save(bb)
        }

        bb.putInt(textures.size)
        for (block in textures) {
            bb.putInt(block.uknI)
            bb.putCString(block.uniformName)

            bb.putInt(block.vars.size)
            for (sv in block.vars) {
                sv.save(bb)
            }
        }

        bb.putLenArray(vertexShader!!)
        bb.putLenArray(fragmentShader!!)
    }

    // TODO reverse-engineer what these variables are
    data class StateVar(val id: Int, val ukn2: Byte, var val4: Int?, var val8: Idstring?) {
        fun save(bb: ByteBuffer) {
            bb.putInt(id)
            bb.put(ukn2)
            if (ukn2 == 0.toByte()) {
                bb.putInt(val4!!)
            } else {
                bb.putLong(val8!!.hash)
            }
        }

        companion object {
            fun load(bb: ByteBuffer): StateVar {
                val id = bb.int

                // Maybe this is a version identifier?
                val varB = bb.get()
                return if (varB == 0.toByte()) {
                    StateVar(id, varB, bb.int, null)
                } else {
                    StateVar(id, varB, null, Idstring(bb.long))
                }
            }
        }
    }

    data class TextureBlock(val uknI: Int, val uniformName: String, val vars: List<StateVar>)
}

class ObjShaderLibrary(hdr: ObjectHeader) : PersistentObject(hdr) {
    val renderTemplates = HashMap<Idstring, ObjShader>()

    override fun load(bb: ByteBuffer, refMap: RefMap) {
        val len = bb.int
        for (i in 1..len) {
            val idstring = Idstring(bb.long)
            val refId = bb.int

            val shader = refMap[refId] as ObjShader
            renderTemplates[idstring] = shader
        }
    }

    override fun save(bb: ByteBuffer) {
        bb.putInt(renderTemplates.size)

        // Note the hashes seem to be sorted in ascending order. I don't think this is required for
        // it to load successfully, but we can use it to get byte-identical outputs to make sure
        // the loading and saving logic works properly.
        // Note we sort on the Idstrings not their hashes, since Java doesn't have unsigned types
        // the values with a true MSB will be considered negative and sort first.
        for ((id, shader) in renderTemplates.entries.sortedBy { it.key }) {
            bb.putLong(id.hash)
            bb.putRefId(shader)
        }
    }
}
