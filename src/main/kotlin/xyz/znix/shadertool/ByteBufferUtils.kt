package xyz.znix.shadertool

import java.nio.ByteBuffer
import java.nio.charset.Charset

fun ByteBuffer.putRefId(obj: PersistentObject) {
    putInt(obj.hdr!!.refId)
}

fun ByteBuffer.getCString(charset: Charset = Charsets.UTF_8): String {
    val bytes = ArrayList<Byte>()

    while (true) {
        val b = get()
        if (b == 0.toByte()) break
        bytes += b
    }

    return bytes.toByteArray().toString(charset)
}

fun ByteBuffer.putCString(str: String, charset: Charset = Charsets.UTF_8) {
    put(str.toByteArray(charset))
    put(0.toByte())
}

fun ByteBuffer.getLenArray(): ByteArray {
    val bytes = ByteArray(int)
    get(bytes)
    return bytes
}

fun ByteBuffer.putLenArray(bytes: ByteArray) {
    putInt(bytes.size)
    put(bytes)
}

