package xyz.znix.shadertool

import java.nio.ByteBuffer
import java.nio.ByteOrder

class ShaderPackage {
    private val objects = ArrayList<PersistentObject>()

    private var frontPadding: Int? = null

    fun load(bytes: ByteArray) {
        val bb = ByteBuffer.wrap(bytes)
        bb.order(ByteOrder.LITTLE_ENDIAN)

        var count = bb.int
        if (count == -1) {
            frontPadding = bb.int
            count = bb.int
        }

        // Load the headers
        val headers = ArrayList<ObjectHeader>()
        for (i in 1..count) {
            val type = bb.int
            val refId = bb.int
            val len = bb.int

            val hdr = ObjectHeader(type, refId, len, bb.position())
            headers += hdr

            bb.position(hdr.end)
        }

        // Create the objects, but don't fill them with data yet
        objects.clear()
        objects += headers.map { it.build() }

        // Create a refmap to look up objects by ID
        val refMap = RefMap()
        for (obj in objects) {
            refMap[obj.hdr!!.refId] = obj
        }

        // Finally load each object
        for (obj in objects) {
            val hdr = obj.hdr!!
            val innerBuff = bb.slice(hdr.pos, hdr.len)
            innerBuff.order(ByteOrder.LITTLE_ENDIAN)

            obj.load(innerBuff, refMap)

            // Make sure all the data was indeed read
            check(!innerBuff.hasRemaining())
        }
    }

    fun save(): ByteArray {
        // Write the whole thing back as a byte-for-byte identical output
        val output = ByteBuffer.allocate((1 shl 20) * 15) // 15MiB should be way more than enough
        output.order(ByteOrder.LITTLE_ENDIAN)

        // This shouldn't have any effect in-game, just required to make it byte-identical
        frontPadding?.let {
            output.putInt(-1)
            output.putInt(it)
        }

        // For temporary use when writing each object
        val itemBuff = ByteBuffer.allocate((1 shl 20) * 15)
        itemBuff.order(ByteOrder.LITTLE_ENDIAN)

        output.putInt(objects.size)
        for (obj in objects) {
            // Write the contents into a temporary buffer, so we can find it's length
            itemBuff.clear()
            obj.save(itemBuff)

            val hdr = obj.hdr!!
            output.putInt(hdr.type)
            output.putInt(hdr.refId)
            output.putInt(itemBuff.position())

            itemBuff.flip()
            output.put(itemBuff)
        }

        output.flip()
        val outputBytes = ByteArray(output.remaining())
        output.get(outputBytes)
        return outputBytes
    }

    fun findLibrary(): ObjShaderLibrary {
        // Find what should be the only shader library
        return objects.mapNotNull { it as? ObjShaderLibrary }.toList().let {
            check(it.size == 1)
            it.first()
        }
    }
}