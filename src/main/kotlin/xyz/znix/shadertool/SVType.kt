package xyz.znix.shadertool

/**
 * Represents a valid state variable ID, and stores information about it
 */
@Suppress("unused")
sealed class SVType(val id: Int, val argType: ArgType) {
    object PolyMode : SVType(0x00, ArgType.Bool)
    object CullMode : SVType(0x01, ArgType.ArgEnum("Back (id0)", "Back (id1)", "Disabled", "Front", "Back (id4)"))
    object DBias : SVType(0x03, ArgType.Float)
    object SBias : SVType(0x05, ArgType.Float)
    object ScissorTest : SVType(0x07, ArgType.Bool)
    object DepthTest : SVType(0x0A, ArgType.Bool)
    object DepthMask : SVType(0x0B, ArgType.Bool)
    object DepthFunc : SVType(0x0C, ArgType.ArgEnum.Cmp)
    object StencilTest : SVType(0x0D, ArgType.Bool)
    object SMask : SVType(0x0E, ArgType.Int)
    object StencilMaskBits : SVType(0x0F, ArgType.Int)
    object SFailFront : SVType(0x10, ArgType.ArgEnum.Op)
    object DFailFront : SVType(0x11, ArgType.ArgEnum.Op)
    object DPassFront : SVType(0x12, ArgType.ArgEnum.Op)
    object SFuncFront : SVType(0x13, ArgType.ArgEnum.Cmp)
    object SFailBack : SVType(0x14, ArgType.ArgEnum.Op)
    object DFailBack : SVType(0x15, ArgType.ArgEnum.Op)
    object DPassBack : SVType(0x16, ArgType.ArgEnum.Op)
    object SFuncBack : SVType(0x17, ArgType.ArgEnum.Cmp)
    object StencilRef : SVType(0x18, ArgType.Int)
    object EnableBlend : SVType(0x1D, ArgType.Bool)
    object BlendSrc : SVType(0x25, ArgType.ArgEnum.Blend)
    object BlendDst : SVType(0x26, ArgType.ArgEnum.Blend)
    object BlendOp : SVType(0x27, ArgType.ArgEnum.BlendOp)
    object SeparateAlpha : SVType(0x28, ArgType.Bool)
    object SrcAlpha : SVType(0x29, ArgType.ArgEnum.Blend)
    object DstAlpha : SVType(0x2A, ArgType.ArgEnum.Blend)
    object OpAlpha : SVType(0x2B, ArgType.ArgEnum.BlendOp)
    object ColourMask : SVType(0x2C, ArgType.Bitmask("Red", "Green", "Blue", "Alpha"))

    companion object {
        fun byId(id: Int): SVType? {
            for (sub in SVType::class.sealedSubclasses) {
                val instance = sub.objectInstance!!
                if (instance.id == id)
                    return instance
            }
            return null
        }
    }
}
