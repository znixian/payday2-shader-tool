package xyz.znix.shadertool

import java.io.File

class Idstring : Comparable<Idstring> {
    val hash: Long

    private var cachedStr: String?
    val str: String
        get() {
            resolve()
            return cachedStr!!
        }

    private var hasStringInternal: Boolean?
    val hasString: Boolean
        get() {
            resolve()
            return hasStringInternal!!
        }

    private fun resolve() {
        if (cachedStr != null) return

        val str = HashList.getNoHex(hash)
        cachedStr = str ?: HashList.hashHex(hash)
        hasStringInternal = str != null
    }

    constructor(fromHash: Long) {
        hash = fromHash
        cachedStr = null
        hasStringInternal = null
    }

    constructor(fromStr: String) {
        hash = Lookup8.dieselHashUTF8(fromStr)
        cachedStr = fromStr
        hasStringInternal = true
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        return hash == (other as Idstring).hash
    }

    override fun hashCode(): Int {
        return hash.toInt()
    }

    override fun toString(): String = str

    override fun compareTo(other: Idstring): Int {
        return java.lang.Long.compareUnsigned(hash, other.hash)
    }
}

object HashList {
    private val hashes = HashMap<Long, String>()

    init {
        val start = System.nanoTime()

        val hashlist = File("/home/znix/Static/Games/MiscPD2/hashlist/dist/full_hashlist").readLines()
        for (line in hashlist) {
            hashes[Lookup8.dieselHashUTF8(line)] = line
        }

        val time = System.nanoTime() - start
        println("Built hashes in ${time / 1_000_000}ms")
    }

    operator fun get(value: Long): String = hashes[value] ?: hashHex(value)
    fun getNoHex(value: Long): String? = hashes[value]
    fun hashHex(value: Long): String = java.lang.Long.toUnsignedString(value, 16)
}