package xyz.znix.shadertool

sealed class ArgType {
    abstract fun strOf(value: ObjShaderPass.StateVar): String

    object Bool : ArgType() {
        override fun strOf(value: ObjShaderPass.StateVar): String = (value.val4!! != 0).toString()
    }

    object Int : ArgType() {
        override fun strOf(value: ObjShaderPass.StateVar): String = value.val4!!.toString()
    }

    object Float : ArgType() {
        override fun strOf(value: ObjShaderPass.StateVar): String {
            return java.lang.Float.intBitsToFloat(value.val4!!).toString()
        }
    }

    open class ArgEnum(vararg values: String?) : ArgType() {
        private val values = values.toList()

        object Cmp : ArgEnum("Never", "Equal", "Less or Equal", "Greater", "Greater or Equal", "Not Equal", "Always")
        object Op : ArgEnum("Keep", null, "Replace", "Increment", "Decrement")
        object Blend : ArgEnum("Zero", "One", "Source Colour", "One-Src", "Src Alpha", "One-Src Alpha",
                "Dest Alpha", "One-Dest Alpha", "Dest Colour", "One-Dest", "Src Alpha (dup)", "Const Colour", "One-Const")

        object BlendOp : ArgEnum("Add", "Subtract", "Reverse-Subtract")

        override fun strOf(value: ObjShaderPass.StateVar): String = values[value.val4!!] ?: error("Bad enum value")
    }

    class Bitmask(vararg names: String) : ArgType() {
        private val names = names.toList()

        override fun strOf(value: ObjShaderPass.StateVar): String {
            var bits = value.val4!!
            val found = ArrayList<String>()
            for ((i, name) in names.withIndex()) {
                val bit = 1 shl i
                if ((bits and bit) == 0) continue
                bits = bits and (bit.inv())
                found += name
            }
            check(bits == 0)
            return found.joinToString()
        }
    }
}
