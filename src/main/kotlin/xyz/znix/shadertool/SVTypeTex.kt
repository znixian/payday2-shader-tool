package xyz.znix.shadertool

@Suppress("unused")
sealed class SVTypeTex(val id: Int, val argType: ArgType) {
    object Filtering : SVTypeTex(0, ArgType.ArgEnum("Nearest", "Linear Mip", "Linear", null, null, null,
            null, null, "Linear Mip with custom Anisotropic Filtering", null, null, "Linear with Depth Testing"))

    object WrapX : SVTypeTex(1, WrapModes)
    object WrapY : SVTypeTex(2, WrapModes)
    object WrapZ : SVTypeTex(3, WrapModes) // For 3D textures
    object DecodeSRGB : SVTypeTex(13, ArgType.Bool)

    companion object {
        fun byId(id: Int): SVTypeTex? {
            for (sub in SVTypeTex::class.sealedSubclasses) {
                val instance = sub.objectInstance!!
                if (instance.id == id)
                    return instance
            }
            return null
        }
    }

    object WrapModes : ArgType.ArgEnum("Repeat", "Clamp to Edge", "Mirrored Repeat (id2)",
            "Clamp to Border - use border colour when out of bounds", "Mirrored Repeat (id4)")
}
