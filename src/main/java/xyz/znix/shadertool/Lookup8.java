package xyz.znix.shadertool;

import kotlin.text.Charsets;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Java implementation of the Lookup8 hash function.
 * <p>
 * See https://burtleburtle.net/bob/c/lookup8.c and http://burtleburtle.net/bob/hash/evahash.html
 */
public class Lookup8 {
    // Note: make sure your editor understands @formatter:on/off before working on this file
    // In IntelliJ you can turn this on under Settings->Editor->Code Style->Formatter Control

    private Lookup8() {
    }

    public static long dieselHashUTF8(String data) {
        return dieselHash(data.getBytes(Charsets.UTF_8));
    }

    public static long dieselHash(byte[] data) {
        return lookup8(data, (byte) 0);
    }

    public static long lookup8(byte[] data, byte level) {
        // Note that due to how two's complement works, there *shouldn't* be any
        // difference (AFAIK) between signed/unsigned addition and subtraction, and
        // xor of course works the same. The only thing to note is that right-shifts
        // must be of the unsigned type (>>>), otherwise they'll set the MSB to the
        // previous MSB to maintain the sign.

        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.LITTLE_ENDIAN);

        LongTriple t = new LongTriple();
        t.a = t.b = level;
        t.c = 0x9e3779b97f4a7c13L; // the golden ratio; an arbitrary value

        while (bb.remaining() >= 24) {
            // Since we set bb to little-endian mode, we don't have to bother with manually copying out the values
            t.a += bb.getLong();
            t.b += bb.getLong();
            t.c += bb.getLong();
            mix(t);
        }

        // Handle the last 23 (at most) bytes
        t.c += data.length; // Toss the length in as an extra input to avoid zero bytes not making a difference

        // When we're using absolute addressing it normally wouldn't be relative to where we're already up to, so
        // make a slice of the ByteBuffer where idx 0 is what get() would return on the original.
        ByteBuffer rb = bb.slice();

        switch (bb.remaining())              // all the case statements fall through
        {
            // @formatter:off
            case 23: t.c+=((long)rb.get(22)<<56);
            case 22: t.c+=((long)rb.get(21)<<48);
            case 21: t.c+=((long)rb.get(20)<<40);
            case 20: t.c+=((long)rb.get(19)<<32);
            case 19: t.c+=((long)rb.get(18)<<24);
            case 18: t.c+=((long)rb.get(17)<<16);
            case 17: t.c+=((long)rb.get(16)<<8);
                /* the first byte of c is reserved for the length */
            case 16: t.b+=((long)rb.get(15)<<56);
            case 15: t.b+=((long)rb.get(14)<<48);
            case 14: t.b+=((long)rb.get(13)<<40);
            case 13: t.b+=((long)rb.get(12)<<32);
            case 12: t.b+=((long)rb.get(11)<<24);
            case 11: t.b+=((long)rb.get(10)<<16);
            case 10: t.b+=((long)rb.get( 9)<<8);
            case  9: t.b+=((long)rb.get( 8));
            case  8: t.a+=((long)rb.get( 7)<<56);
            case  7: t.a+=((long)rb.get( 6)<<48);
            case  6: t.a+=((long)rb.get( 5)<<40);
            case  5: t.a+=((long)rb.get( 4)<<32);
            case  4: t.a+=((long)rb.get( 3)<<24);
            case  3: t.a+=((long)rb.get( 2)<<16);
            case  2: t.a+=((long)rb.get( 1)<<8);
            case  1: t.a+=((long)rb.get( 0));
                /* case 0: nothing left to add */
            // @formatter:on
        }
        mix(t);

        return t.c;
    }

    private static void mix(LongTriple t) {
        long a = t.a, b = t.b, c = t.c;
        // @formatter:off
        a -= b; a -= c; a ^= (c>>>43);
        b -= c; b -= a; b ^= (a<<9);
        c -= a; c -= b; c ^= (b>>>8);
        a -= b; a -= c; a ^= (c>>>38);
        b -= c; b -= a; b ^= (a<<23);
        c -= a; c -= b; c ^= (b>>>5);
        a -= b; a -= c; a ^= (c>>>35);
        b -= c; b -= a; b ^= (a<<49);
        c -= a; c -= b; c ^= (b>>>11);
        a -= b; a -= c; a ^= (c>>>12);
        b -= c; b -= a; b ^= (a<<18);
        c -= a; c -= b; c ^= (b>>>22);
        // @formatter:on
        t.a = a;
        t.b = b;
        t.c = c;
    }

    private static class LongTriple {
        long a, b, c;
    }
}
